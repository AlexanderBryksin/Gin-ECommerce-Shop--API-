# Go Gin MongoDB REST API Example

> Go Gin MongoDB REST API Example Simple e-shop example. Authentication by Authorization Header Bearer token or cookie with role based routes protection. Create, update, delete and get requests with pagination and sorting params.

## Usage

Rename ".env.example" to ".env" and update the values/settings to your own



## API Documentation

Documentation with examples [link](https://documenter.getpostman.com/view/4199988/SW11Xdy3?version=latest)

## Used
* [Gin](https://github.com/gin-gonic/gin)
* [mongo-go-driver](https://github.com/mongodb/mongo-go-driver)
* [jwt-go](https://github.com/dgrijalva/jwt-go)
* [bcryptjs](https://godoc.org/golang.org/x/crypto/bcrypt) 
* [validator](https://github.com/go-playground/validator)
* [mongodb](https://www.mongodb.com/blog/post/mongodb-go-driver-tutorial)
* [godotenv](https://github.com/joho/godotenv)



##### Author [Alexander Bryksin](https://github.com/AleksK1NG)
package main

import (
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/server"
	"github.com/joho/godotenv"
	"log"
)

func init() {
	// Load .env file
	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	// Run server
	server.Run()
}

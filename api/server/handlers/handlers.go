package handlers

import (
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/controllers"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/controllers/category_controller"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/controllers/item_controller"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/controllers/user_controller"
)

// Api root handler struct
type ApiHandlers struct {
	UserController     controllers.UserController
	ItemsController    controllers.ItemsController
	CategoryController controllers.CategoryController
}

// Create New api root handlers struct
func NewApiHandlers() *ApiHandlers {
	userController := user_controller.NewUserController()
	itemController := item_controller.NewItemController()
	categoryController := category_controller.NewCategoryController()

	return &ApiHandlers{
		UserController:     userController,
		ItemsController:    itemController,
		CategoryController: categoryController,
	}
}

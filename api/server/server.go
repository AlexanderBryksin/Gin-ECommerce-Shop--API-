package server

import (
	"context"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/db"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/routes"
	handlers2 "github.com/aleksk1ng/gin-ecommerce-shop--api/api/server/handlers"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"net/http"
	"os"
	"time"
)

func Run() {
	log.Println("Starting GO Server ...")

	// Init MongoDB and check connection
	dbClient := db.InitNewMongoDB()
	if err := dbClient.Ping(context.Background(), readpref.Primary()); err != nil {
		log.Fatal("Error on Mongo Ping connection ", err)
	}
	log.Println("MongoDB is Connected !!! ")
	defer dbClient.Disconnect(context.Background())

	handlers := handlers2.NewApiHandlers()

	port := os.Getenv("PORT")
	// Init Router
	r := routes.InitRoutes(handlers)

	// Init Server
	s := &http.Server{
		Addr:           port,
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Fatal(s.ListenAndServe())
}

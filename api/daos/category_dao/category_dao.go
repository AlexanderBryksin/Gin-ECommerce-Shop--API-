package category_dao

import (
	"context"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/db"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/category_model"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/utils/errors_utils"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/utils/query_findOptions"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"time"
)

type CategoryDao struct {
	DbName     string
	Collection string
}

// Init Category Dao
func InitUCategoryDao() *CategoryDao {
	return &CategoryDao{
		DbName:     os.Getenv("DB_NAME"),
		Collection: "category",
	}
}

// TODO: Add Filters from query params
// Find All Categories
func (cd *CategoryDao) GetAllCategories(query map[string]string) ([]*category_model.Category, error) {
	DB := db.GetDB()
	collection := DB.Database(cd.DbName).Collection(cd.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	findOptions, err := query_findOptions.CreateFindOptions(query)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	cursor, err := collection.Find(ctx, bson.M{}, findOptions)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}
	defer cursor.Close(ctx)

	var categories []*category_model.Category
	for cursor.Next(ctx) {
		var category category_model.Category
		if err := cursor.Decode(&category); err != nil {
			return nil, errors_utils.ErrServerError
		}

		categories = append(categories, &category)
	}

	return categories, nil
}

// Create Category
func (cd *CategoryDao) CreateCategory(cat category_model.Category) (*category_model.Category, error) {
	DB := db.GetDB()
	collection := DB.Database(cd.DbName).Collection(cd.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := collection.FindOne(ctx, bson.M{"name": cat.Name}).Decode(&category_model.Category{}); err == nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	result, err := collection.InsertOne(ctx, &cat)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	id := result.InsertedID.(primitive.ObjectID)
	cat.ID = id

	return &cat, nil
}

// Get category by ID
func (cd *CategoryDao) GetCategoryByID(catId string) (*category_model.Category, error) {
	idFromHex, err := primitive.ObjectIDFromHex(catId)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	DB := db.GetDB()
	collection := DB.Database(cd.DbName).Collection(cd.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var category category_model.Category
	if err = collection.FindOne(ctx, bson.M{"_id": idFromHex}).Decode(&category); err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	return &category, nil
}

// Update category in database
func (cd *CategoryDao) UpdateCategory(cat category_model.Category) (*category_model.Category, error) {
	DB := db.GetDB()
	collection := DB.Database(cd.DbName).Collection(cd.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	updateOptions := options.FindOneAndUpdate()
	updateOptions.SetReturnDocument(options.ReturnDocument(1))

	if err := collection.FindOneAndUpdate(ctx, bson.M{"_id": cat.ID}, bson.M{"$set": cat}, updateOptions).Decode(&cat); err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	return &cat, nil
}

// Delete category by id
func (cd *CategoryDao) DeleteCategory(catId string) (*category_model.Category, error) {
	idFromHex, err := primitive.ObjectIDFromHex(catId)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	DB := db.GetDB()
	collection := DB.Database(cd.DbName).Collection(cd.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err = collection.FindOneAndDelete(ctx, bson.M{"_id": idFromHex}).Decode(&category_model.Category{}); err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	return &category_model.Category{}, nil
}

package daos

import (
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/category_model"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/item_model"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/user_model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Items DAO interface
type ItemDao interface {
	CreateItem(item item_model.Item) (*item_model.Item, error)
	GetAllItems(query map[string]string) ([]*item_model.Item, error)
	GetItemByID(itemID string) (*item_model.Item, error)
	UpdateItem(item item_model.Item) (*item_model.Item, error)
	DeleteItem(itemId string) (*item_model.Item, error)
	FindItemByName(searchParam string, query map[string]string) ([]*item_model.Item, error)
}

// User DAO interface
type UserDao interface {
	CreateUser(user user_model.User) (*user_model.User, error)
	GetUserById(id string) (*user_model.User, error)
	GetUserByEmail(email string) (*user_model.User, error)
	UpdateUser(user user_model.User) (*user_model.User, error)
	DeleteUser(userId primitive.ObjectID) (*user_model.User, error)
}

// Category DAO interface
type CategoryDao interface {
	GetAllCategories(query map[string]string) ([]*category_model.Category, error)
	CreateCategory(cat category_model.Category) (*category_model.Category, error)
	GetCategoryByID(catId string) (*category_model.Category, error)
	UpdateCategory(cat category_model.Category) (*category_model.Category, error)
	DeleteCategory(catId string) (*category_model.Category, error)
}

package item_dao

import (
	"context"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/db"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/item_model"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/utils/errors_utils"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/utils/query_findOptions"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"time"
)

// Item DAO
type ItemDao struct {
	DbName     string
	Collection string
}

// Init new item dao instance
func NewItemDao() *ItemDao {
	return &ItemDao{
		DbName:     os.Getenv("DB_NAME"),
		Collection: "items",
	}
}

// Create item in database
func (id *ItemDao) CreateItem(item item_model.Item) (*item_model.Item, error) {
	DB := db.GetDB()
	collection := DB.Database(id.DbName).Collection(id.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	item.SetTimeStamps()

	result, err := collection.InsertOne(ctx, &item)
	if err != nil {
		return nil, errors_utils.ErrServerError
	}

	objectID := result.InsertedID.(primitive.ObjectID)
	item.ID = objectID

	return &item, nil
}

// Get All items from database
func (id *ItemDao) GetAllItems(query map[string]string) ([]*item_model.Item, error) {
	DB := db.GetDB()
	collection := DB.Database(id.DbName).Collection(id.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	findOptions, err := query_findOptions.CreateFindOptions(query)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	cursor, err := collection.Find(ctx, bson.M{}, findOptions)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	var items []*item_model.Item
	if err = cursor.All(ctx, &items); err != nil {
		return nil, errors_utils.ErrServerError
	}

	return items, nil
}

// Get item by id
func (id *ItemDao) GetItemByID(itemID string) (*item_model.Item, error) {
	DB := db.GetDB()
	collection := DB.Database(id.DbName).Collection(id.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	objectID, err := primitive.ObjectIDFromHex(itemID)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	var item item_model.Item
	if err = collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&item); err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	return &item, nil
}

// Update Shop item in database
func (id *ItemDao) UpdateItem(item item_model.Item) (*item_model.Item, error) {
	DB := db.GetDB()
	collection := DB.Database(id.DbName).Collection(id.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	updateOptions := options.FindOneAndUpdate()
	updateOptions.SetReturnDocument(options.ReturnDocument(1))

	if err := collection.FindOneAndUpdate(ctx, bson.M{"_id": item.ID}, bson.M{"$set": item}, updateOptions).Decode(&item); err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	return &item, nil
}

// Delete item from database
func (id *ItemDao) DeleteItem(itemId string) (*item_model.Item, error) {
	DB := db.GetDB()
	collection := DB.Database(id.DbName).Collection(id.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	objectID, err := primitive.ObjectIDFromHex(itemId)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	if err = collection.FindOneAndDelete(ctx, bson.M{"_id": objectID}).Decode(&item_model.Item{}); err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	return &item_model.Item{}, nil
}

// Get item by id
func (id *ItemDao) FindItemByName(searchParam string, query map[string]string) ([]*item_model.Item, error) {
	DB := db.GetDB()
	collection := DB.Database(id.DbName).Collection(id.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var items []*item_model.Item

	findOptions, err := query_findOptions.CreateFindOptions(query)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	cursor, err := collection.Find(ctx, bson.M{"name": searchParam}, findOptions)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	if err = cursor.All(ctx, &items); err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	return items, nil
}

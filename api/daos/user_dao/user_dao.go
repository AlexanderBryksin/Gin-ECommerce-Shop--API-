package user_dao

import (
	"context"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/db"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/user_model"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/utils/errors_utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"time"
)

type UserDao struct {
	DbName     string
	Collection string
}

// Init User Dao
func InitUserDao() *UserDao {
	return &UserDao{
		DbName:     os.Getenv("DB_NAME"),
		Collection: "users",
	}
}

// Create user in db
func (ud *UserDao) CreateUser(user user_model.User) (*user_model.User, error) {
	DB := db.GetDB()
	collection := DB.Database(ud.DbName).Collection(ud.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Check exists or not
	var check user_model.User
	if err := collection.FindOne(ctx, bson.M{"email": user.Email}).Decode(&check); err == nil {
		return nil, errors_utils.ErrUserWithEmailAlreadyExist
	}

	if check.Email != "" {
		return nil, errors_utils.ErrInvalidEmailOrPassword
	}

	// Create user in db
	result, err := collection.InsertOne(ctx, &user)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	id := result.InsertedID.(primitive.ObjectID)
	user.ID = id

	return &user, nil
}

// Get user by id
func (ud *UserDao) GetUserById(id string) (*user_model.User, error) {
	DB := db.GetDB()
	collection := DB.Database(ud.DbName).Collection(ud.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
	defer cancel()

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	var user user_model.User
	if err = collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&user); err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	return &user, nil
}

// Get user by email
func (ud *UserDao) GetUserByEmail(email string) (*user_model.User, error) {
	DB := db.GetDB()
	collection := DB.Database(ud.DbName).Collection(ud.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
	defer cancel()

	var user user_model.User
	if err := collection.FindOne(ctx, bson.M{"email": email}).Decode(&user); err != nil {
		return nil, errors_utils.ErrNoResult
	}

	return &user, nil
}

// Update user data in database
func (ud *UserDao) UpdateUser(user user_model.User) (*user_model.User, error) {
	DB := db.GetDB()
	collection := DB.Database(ud.DbName).Collection(ud.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
	defer cancel()

	updateOptions := options.FindOneAndUpdate()
	updateOptions.SetReturnDocument(options.ReturnDocument(1))

	if err := collection.FindOneAndUpdate(ctx, bson.M{"_id": user.ID}, bson.M{"$set": user}, updateOptions).Decode(&user); err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	return &user, nil
}

// Delete user from database
func (ud *UserDao) DeleteUser(userId primitive.ObjectID) (*user_model.User, error) {
	DB := db.GetDB()
	collection := DB.Database(ud.DbName).Collection(ud.Collection)

	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
	defer cancel()

	if err := collection.FindOneAndDelete(ctx, bson.M{"_id": userId}).Decode(&user_model.User{}); err != nil {
		return nil, errors_utils.ErrStatusBadRequest
	}

	return &user_model.User{}, nil
}

package auth_middleware

import (
	"fmt"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/daos/user_dao"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/user_model"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"log"
	"os"
	"strings"
)

// Load user from Authorization header token or from cookie token
func LoadUserMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get token form header
		bearerHeader := c.Request.Header.Get("Authorization")
		if bearerHeader != "" {
			headerParts := strings.Split(bearerHeader, " ")
			if len(headerParts) == 2 {
				tokenString := headerParts[1]

				ValidateJWTToken(c, tokenString)
				c.Next()
			}
		} else {
			cookie, err := c.Cookie("jwt-token")
			if err != nil || cookie == "" {
				return
			}

			ValidateJWTToken(c, cookie)
			log.Println("load user middleware cookie ", cookie)
			c.Next()
		}
	}
}

// Protect route, check for exists authorized user
func EnforceAuthenticatedMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		user, ok := c.Get("user")
		if ok && user != nil {
			c.Next()
		} else {
			c.AbortWithStatusJSON(401, map[string]string{"Error": "Unauthorized"})
			return
		}
	}
}

// Role based authorization middleware
func RoleAuthMiddleware(roles ...string) gin.HandlerFunc {
	return func(c *gin.Context) {
		user, ok := c.Get("user")
		if !ok {
			c.AbortWithStatusJSON(401, map[string]string{"Error": "Unauthorized"})
			return
		}

		castedUser, ok := user.(*user_model.User)
		if !ok {
			c.AbortWithStatusJSON(401, map[string]string{"Error": "Unauthorized"})
			return
		}

		for _, role := range roles {
			if role == castedUser.Role {
				c.Next()
				return
			}
		}
		log.Println("RoleAuthMiddleware invalid role ", castedUser.Role)
		c.AbortWithStatusJSON(401, map[string]string{"Error": "Unauthorized"})
	}
}

// Validate jwt token, decode user id, find in database and pass by context user struct
func ValidateJWTToken(c *gin.Context, tokenString string) {
	// Parse, validate, and return a token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (tkn interface{}, err error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signin method %v", token.Header["alg"])
		}
		secret := []byte(os.Getenv("JWT_SECRET_KEY"))
		return secret, nil
	})
	if err != nil {
		return
	}

	if !token.Valid {
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		userId, ok := claims["id"].(string)
		fmt.Printf("[+] Authenticated request, authenticated user id is %d\n", userId)
		if !ok {
			return
		}

		userDao := user_dao.InitUserDao()

		user, err := userDao.GetUserById(userId)
		if err != nil {
			return
		}
		log.Println("LoadUserMiddleware ", user)
		c.Set("user", user)
		c.Set("user_id", userId)

	}
}

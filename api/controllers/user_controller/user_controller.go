package user_controller

import (
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/daos"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/daos/user_dao"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/response_model"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/user_model"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/utils/errors_utils"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/utils/jwt_util"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/validators"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

// User Controller struct
type UserController struct {
	userDao daos.UserDao
}

// Init user_model controller instance
func NewUserController() *UserController {
	userDao := user_dao.InitUserDao()
	return &UserController{
		userDao: userDao,
	}
}

// Register new user_model
func (u *UserController) RegisterUser(c *gin.Context) {
	var user user_model.User

	if err := c.BindJSON(&user); err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}

	// validate user
	if err := validators.ValidateRegisterUser(user); err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}

	// add timestamps
	user.SetTimeStamps()
	// hash password
	user.HashUserPassword()

	// create new user
	createdUser, err := u.userDao.CreateUser(user)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	// generate token
	token, err := jwt_util.GenerateJWT(createdUser)
	if err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrServerError, http.StatusInternalServerError)
		return
	}

	// set cookie token
	c.SetCookie("jwt-token", token, 60*60*24, "/", "", false, true)

	// response with token
	u.ResponseWithToken(c, createdUser, token, http.StatusCreated)
}

// Login user with email and password
func (u *UserController) LoginUser(c *gin.Context) {
	var user user_model.User

	if err := c.BindJSON(&user); err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrInvalidEmailOrPassword, http.StatusBadRequest)
		return
	}

	// validate user
	if err := validators.ValidateLoginUser(user); err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrInvalidEmailOrPassword, http.StatusBadRequest)
		return
	}

	// find user
	findedUser, err := u.userDao.GetUserByEmail(user.Email)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	// verify compare passwords
	if err = findedUser.VerifyUserPassword(user.Password); err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrInvalidEmailOrPassword, http.StatusBadRequest)
		return
	}

	// generate jwt token
	token, err := jwt_util.GenerateJWT(findedUser)
	if err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrServerError, http.StatusInternalServerError)
		return
	}

	// set cookie token
	c.SetCookie("jwt-token", token, 60*60*24, "/", "", false, true)

	// response with user
	u.ResponseWithToken(c, findedUser, token, http.StatusOK)
}

// Load current user_model with auth middleware
func (u *UserController) LoadCurrentUser(c *gin.Context) {
	user, ok := c.Get("user")
	if !ok {
		errors_utils.ErrorResponse(c, errors_utils.ErrInvalidEmailOrPassword, http.StatusBadRequest)
		return
	}

	// cast user from interface{} to struct for response func
	castedUser, ok := user.(*user_model.User)
	if !ok {
		errors_utils.ErrorResponse(c, errors_utils.ErrServerError, http.StatusBadRequest)
		return
	}

	u.ResponseWithUser(c, castedUser, http.StatusOK)
}

// Logout user_model and clean cookie
func (u *UserController) LogoutUser(c *gin.Context) {
	// delete cookie token
	c.SetCookie("jwt-token", "", -1, "/", "", false, true)
}

// Update user_model
func (u *UserController) UpdateUser(c *gin.Context) {
	var user user_model.User
	userId := c.Param("id")

	idFromHex, err := primitive.ObjectIDFromHex(userId)
	if err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}

	if err := c.BindJSON(&user); err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}
	// add user id from query params
	user.ID = idFromHex

	updatedUser, err := u.userDao.UpdateUser(user)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	u.ResponseWithUser(c, updatedUser, http.StatusOK)
}

// Delete user_model
func (u *UserController) DeleteUser(c *gin.Context) {
	userId := c.Param("id")

	// cast to ObjectID from string
	idFromHex, err := primitive.ObjectIDFromHex(userId)
	if err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}

	deletedUser, err := u.userDao.DeleteUser(idFromHex)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{"message": "User deleted", "data": deletedUser})
}

// Response with user struct and token, removed password
func (u *UserController) ResponseWithToken(c *gin.Context, user *user_model.User, token string, statusCode int) {
	// create response
	user.Password = ""
	response := response_model.UserWithToken{
		User:  user,
		Token: token,
	}

	c.JSON(statusCode, response)

}

// Response with user struct, and remove password
func (u *UserController) ResponseWithUser(c *gin.Context, user *user_model.User, statusCode int) {
	user.Password = ""
	c.JSON(statusCode, user)
}

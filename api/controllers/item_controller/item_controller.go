package item_controller

import (
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/daos"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/daos/item_dao"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/item_model"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/utils/errors_utils"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

// Item Controller struct
type ItemController struct {
	iDao daos.ItemDao
}

// Create New item controller instance
func NewItemController() *ItemController {
	itemDao := item_dao.NewItemDao()
	return &ItemController{
		iDao: itemDao,
	}
}

// Create new item in database, /api/v1/category/:id/items
func (ic *ItemController) CreateItem(c *gin.Context) {
	categoryId := c.Param("id")
	idFromHex, err := primitive.ObjectIDFromHex(categoryId)
	if err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}

	var item item_model.Item
	item.CategoryID = idFromHex

	if err := c.BindJSON(&item); err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}

	createdItem, err := ic.iDao.CreateItem(item)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusCreated, createdItem)
}

// Get All Items from database
func (ic *ItemController) GetAllItems(c *gin.Context) {
	limit := c.Query("limit")
	skip := c.Query("skip")
	sortBy := c.Query("sortBy")

	queryOptions := map[string]string{
		"limit":  limit,
		"skip":   skip,
		"sortBy": sortBy,
	}

	allItems, err := ic.iDao.GetAllItems(queryOptions)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, allItems)
}

// Get Item by id query param
func (ic *ItemController) GetItemByID(c *gin.Context) {
	itemId := c.Param("id")

	item, err := ic.iDao.GetItemByID(itemId)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, item)
}

// Update item by id query param
func (ic *ItemController) UpdateItem(c *gin.Context) {
	itemId := c.Param("id")
	objectID, err := primitive.ObjectIDFromHex(itemId)
	if err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}

	var item item_model.Item
	if err = c.BindJSON(&item); err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}

	item.ID = objectID

	updatedItem, err := ic.iDao.UpdateItem(item)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, updatedItem)
}

// Delete item by id query param
func (ic *ItemController) DeleteItem(c *gin.Context) {
	itemId := c.Param("id")

	deletedItem, err := ic.iDao.DeleteItem(itemId)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{"message": "Item deleted", "data": deletedItem})
}

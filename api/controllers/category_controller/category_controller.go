package category_controller

import (
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/daos"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/daos/category_dao"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/category_model"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/utils/errors_utils"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

// Category Controller struct
type CategoryController struct {
	catDao daos.CategoryDao
}

// Init category controller instance
func NewCategoryController() *CategoryController {
	catDao := category_dao.InitUCategoryDao()
	return &CategoryController{
		catDao: catDao,
	}
}

func (cc *CategoryController) CreateCategory(c *gin.Context) {
	var category category_model.Category

	if err := c.BindJSON(&category); err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}

	category.SetTimeStamps()

	createdCat, err := cc.catDao.CreateCategory(category)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusCreated, createdCat)
}

// Get all categories
func (cc *CategoryController) GetAllCategories(c *gin.Context) {

	queryOptions := map[string]string{
		"limit":  c.Query("limit"),
		"skip":   c.Query("skip"),
		"sortBy": c.Query("sortBy"),
	}

	allCategories, err := cc.catDao.GetAllCategories(queryOptions)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, allCategories)
}

// Get category by ID
func (cc *CategoryController) GetCategoryByID(c *gin.Context) {
	catId := c.Param("id")

	category, err := cc.catDao.GetCategoryByID(catId)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, category)
}

// Update category in database
func (cc *CategoryController) UpdateCategory(c *gin.Context) {
	catId := c.Param("id")
	var category category_model.Category

	idFromHex, err := primitive.ObjectIDFromHex(catId)
	if err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}

	if err = c.BindJSON(&category); err != nil {
		errors_utils.ErrorResponse(c, errors_utils.ErrStatusBadRequest, http.StatusBadRequest)
		return
	}

	category.ID = idFromHex

	updatedCategory, err := cc.catDao.UpdateCategory(category)
	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, updatedCategory)
}

// Delete category from database
func (cc *CategoryController) DeleteCategory(c *gin.Context) {
	catId := c.Param("id")
	deletedCategory, err := cc.catDao.DeleteCategory(catId)

	if err != nil {
		errors_utils.ErrorResponse(c, err, http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{"message": "category deleted", "data": deletedCategory})
}

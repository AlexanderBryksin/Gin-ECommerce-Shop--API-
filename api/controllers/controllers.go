package controllers

import "github.com/gin-gonic/gin"

// User controller interface
type UserController interface {
	RegisterUser(c *gin.Context)
	LoginUser(c *gin.Context)
	LoadCurrentUser(c *gin.Context)
	LogoutUser(c *gin.Context)
	UpdateUser(c *gin.Context)
	DeleteUser(c *gin.Context)
}

// Items controller interface
type ItemsController interface {
	CreateItem(c *gin.Context)
	GetAllItems(c *gin.Context)
	GetItemByID(c *gin.Context)
	UpdateItem(c *gin.Context)
	DeleteItem(c *gin.Context)
}

// Categories controller interface
type CategoryController interface {
	CreateCategory(c *gin.Context)
	GetAllCategories(c *gin.Context)
	GetCategoryByID(c *gin.Context)
	UpdateCategory(c *gin.Context)
	DeleteCategory(c *gin.Context)
}

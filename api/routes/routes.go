package routes

import (
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/middlewares/auth_middleware"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/server/handlers"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
)

// Create Initial Router Engine
func InitRoutes(h *handlers.ApiHandlers) *gin.Engine {
	// init router engine
	r := gin.Default()

	// Middlewares
	r.Use(cors.Default())
	r.Use(gzip.Gzip(gzip.DefaultCompression))
	r.Use(auth_middleware.LoadUserMiddleware())

	// route groups
	v1 := r.Group("/api/v1")

	// Auth routes group /api/v1/auth
	authRoutes := v1.Group("/auth")

	authRoutes.GET("/me", h.UserController.LoadCurrentUser)
	authRoutes.POST("/register", h.UserController.RegisterUser)
	authRoutes.POST("/login", h.UserController.LoginUser)
	authRoutes.GET("/logout", h.UserController.LogoutUser)

	authRoutesById := authRoutes.Group("/user")
	{
		authRoutesById.Use(auth_middleware.EnforceAuthenticatedMiddleware())
		authRoutesById.PUT("/:id", h.UserController.UpdateUser)
		authRoutesById.DELETE("/:id", h.UserController.DeleteUser)
	}

	// Category group
	categoryRoutes := v1.Group("/category")
	{
		categoryRoutes.GET("/", h.CategoryController.GetAllCategories)
		categoryRoutes.GET("/:id", h.CategoryController.GetCategoryByID)

		categoryRoutes.Use(auth_middleware.EnforceAuthenticatedMiddleware())
		categoryRoutes.Use(auth_middleware.RoleAuthMiddleware("admin"))

		categoryRoutes.POST("/", h.CategoryController.CreateCategory)
		categoryRoutes.PUT("/:id", h.CategoryController.UpdateCategory)
		categoryRoutes.DELETE("/:id", h.CategoryController.DeleteCategory)
	}

	// Category Items Group
	categoryItemsRoutes := v1.Group("/category/:id/items")
	{
		categoryItemsRoutes.GET("/", h.ItemsController.GetAllItems)

		categoryItemsRoutes.Use(auth_middleware.EnforceAuthenticatedMiddleware())
		categoryItemsRoutes.Use(auth_middleware.RoleAuthMiddleware("admin"))

		categoryItemsRoutes.POST("/", h.ItemsController.CreateItem)
	}

	// Items Group
	itemsRoutes := v1.Group("/items")
	{
		itemsRoutes.GET("/:id", h.ItemsController.GetItemByID)

		itemsRoutes.Use(auth_middleware.EnforceAuthenticatedMiddleware())
		itemsRoutes.Use(auth_middleware.RoleAuthMiddleware("admin"))

		itemsRoutes.PUT("/:id", h.ItemsController.UpdateItem)
		itemsRoutes.DELETE("/:id", h.ItemsController.DeleteItem)
	}

	return r
}

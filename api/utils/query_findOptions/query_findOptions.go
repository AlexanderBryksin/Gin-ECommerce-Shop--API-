package query_findOptions

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"strconv"
)

// Find options factory
func CreateFindOptions(query map[string]string) (*options.FindOptions, error) {
	findOptions := options.Find()

	findOptions.SetSkip(0)
	findOptions.SetLimit(10)
	findOptions.SetSort(bson.M{"updated_at": -1})

	limitQuery, ok := query["limit"]
	if ok && limitQuery != "" {
		limit, err := strconv.ParseInt(limitQuery, 10, 64)
		if err != nil {
			return findOptions, err
		}
		findOptions.SetLimit(limit)
	}

	skipQuery, ok := query["skip"]
	if ok && skipQuery != "" {
		skip, err := strconv.ParseInt(skipQuery, 10, 64)
		if err != nil {
			return findOptions, err
		}
		findOptions.SetSkip(skip)
	}

	sortQuery, ok := query["sortBy"]
	if ok && sortQuery != "" {
		findOptions.SetSort(bson.M{sortQuery: -1})
	}

	return findOptions, nil
}

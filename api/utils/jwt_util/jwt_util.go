package jwt_util

import (
	"errors"
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/user_model"
	"github.com/dgrijalva/jwt-go"
	"html"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

// JWT Claims struct
type Claims struct {
	Email string `json:"email"`
	ID    string `json:"id"`
	jwt.StandardClaims
}

// Generate JWT
func GenerateJWT(user *user_model.User) (string, error) {
	jwtSecrerKey := os.Getenv("JWT_SECRET_KEY")

	// Create the JWT claims, which includes the username and expiry time
	claims := &Claims{
		Email: user.Email,
		ID:    user.ID.Hex(),
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * 60).Unix(),
		},
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Create the JWT string
	tokenString, err := token.SignedString([]byte(jwtSecrerKey))
	if err != nil {
		log.Println("Error on token.SignedString ", err)
		return "", err
	}

	return tokenString, nil

}

// Extract JWT token from request body
func ExtractJWT(r *http.Request) (map[string]interface{}, error) {
	// Get the JWT string
	tknStr := ExtractBearerToken(r)

	// Initialize a new instance of `Claims` (here using Claims map)
	claims := jwt.MapClaims{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	token, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (jwtKey interface{}, err error) {
		return jwtKey, err
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return nil, err
		}
		return nil, err
	}

	if !token.Valid {
		return nil, errors.New("Invalid token ")
	}

	return claims, nil
}

// Extract bearer token from request Authorization header
func ExtractBearerToken(r *http.Request) string {
	headerAuthorization := r.Header.Get("Authorization")
	bearerToken := strings.Split(headerAuthorization, " ")
	return html.EscapeString(bearerToken[1])
}

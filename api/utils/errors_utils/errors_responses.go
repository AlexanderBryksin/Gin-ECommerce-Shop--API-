package errors_utils

import (
	"errors"
	"github.com/gin-gonic/gin"
	"log"
)

const (
	EmailErrorMessage             = "user with given email already exists"
	ServerErrorMessage            = "server error"
	InvalidCredentialsMessage     = "invalid credentials"
	InvalidEmailOrPasswordMessage = "Invalid email or password"
	CategoryName                  = "category with given name is already created"
)

var (
	ErrNoResult                     = errors.New("no result")
	ErrUserWithEmailAlreadyExist    = errors.New("user with email already exist")
	ErrUserWithUsernameAlreadyExist = errors.New("user with username already exist")
	ErrServerError                  = errors.New("server error")
	ErrStatusBadRequest             = errors.New("bad request")
	ErrInvalidEmailOrPassword       = errors.New("invalid email or password")
)

// ErrorResponse
func ErrorResponse(c *gin.Context, err error, statusCode int) {
	// log error
	log.Println("Error response ", err.Error())

	// respond with json error
	c.AbortWithStatusJSON(statusCode, map[string]string{"message": err.Error()})
	return
}

package response_model

import "github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/user_model"

type UserWithToken struct {
	User  *user_model.User `json:"user"`
	Token string           `json:"token"`
}

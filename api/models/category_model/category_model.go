package category_model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

// Category Model
type Category struct {
	ID          primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name        string             `json:"name,omitempty" bson:"name,omitempty" validate:"required"`
	Description string             `json:"description,omitempty" bson:"description,omitempty" validate:"required, gte=10,lte=240"`
	ImageURL    string             `json:"imageURL" bson:"imageURL" validate:"required,url"`
	CreatedAt   time.Time          `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt   time.Time          `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	// UserID        primitive.ObjectID `json:"user_id,omitempty" json:"user_id,omitempty"`
}

// Set CreatedAt and UpdatedAt fields
func (c *Category) SetTimeStamps() {
	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()
}

package item_model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

// Item Model
type Item struct {
	ID          primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name        string             `json:"name,omitempty" bson:"name,omitempty" validate:"required"`
	Description string             `json:"description,omitempty" bson:"description,omitempty" validate:"required, gte=10,lte=240"`
	Price       int                `json:"price,omitempty" bson:"price,omitempty" validate:"required"`
	ImageURL    string             `json:"imageURL" bson:"imageURL" validate:"required,url"`
	CreatedAt   time.Time          `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt   time.Time          `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	CategoryID  primitive.ObjectID `json:"category_id,omitempty" json:"category_id,omitempty" validate:"required"`
}

// Set CreatedAt and UpdatedAt fields
func (c *Item) SetTimeStamps() {
	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()
}

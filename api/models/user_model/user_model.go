package user_model

import (
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/utils/security"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

// User model
type User struct {
	ID        primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name      string             `json:"name,omitempty" bson:"name,omitempty" validate:"required"`
	Role      string             `json:"role,omitempty" bson:"role,omitempty" validate:"required"`
	Email     string             `json:"email" bson:"email" validate:"required,email"`
	Password  string             `json:"password,omitempty" bson:"password,omitempty" validate:"gte=6,lte=24"`
	CreatedAt time.Time          `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt time.Time          `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}

// Set CreatedAt and UpdatedAt fields
func (u *User) SetTimeStamps() {
	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()
}

// Hash user password
func (u *User) HashUserPassword() {
	hashPassword, err := security.HashPassword(u.Password)
	if err != nil {
		return
	}
	u.Password = string(hashPassword)
}

// Verify compare user hashed password (from db) and password string from request
func (u *User) VerifyUserPassword(password string) error {
	return security.VerifyPassword(u.Password, password)
}

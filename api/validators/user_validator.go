package validators

import (
	"github.com/aleksk1ng/gin-ecommerce-shop--api/api/models/user_model"
	"gopkg.in/go-playground/validator.v9"
)

// Validate User struct fields
func ValidateRegisterUser(u user_model.User) error {
	validate := validator.New()

	if err := validate.Struct(&u); err != nil {
		return err
	}

	return nil
}

// Validate login request with email and password
func ValidateLoginUser(user user_model.User) error {
	validate := validator.New()

	if err := validate.Var(user.Email, "required,email"); err != nil {
		return err
	}
	if err := validate.Var(user.Password, "required,gte=6,lte=24"); err != nil {
		return err
	}

	return nil
}

// Validate exists custom user struct fields
func ValidateCustomUserFields(user user_model.User) error {
	validate := validator.New()

	if user.Email != "" {
		if err := validate.Var(user.Email, "required,email"); err != nil {
			return err
		}
	}

	if user.Password != "" {
		if err := validate.Var(user.Password, "required,gte=6,lte=24"); err != nil {
			return err
		}
	}

	return nil
}

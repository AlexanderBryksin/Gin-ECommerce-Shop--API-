package db

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
	"time"
)

var MongoDB *mongo.Client

// Init MongoDB instance
func InitNewMongoDB() *mongo.Client {
	clientOptions := options.Client().ApplyURI(os.Getenv("MONGO_URI"))

	// Set retry writes, required for MlaB
	clientOptions.SetRetryWrites(false)

	// Init MongoDB Client
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		log.Fatal("Error loading Mongo NewClient options ", err)
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	if err = client.Connect(ctx); err != nil {
		log.Fatal("Error on Mongo client.Connect ", err)
	}

	MongoDB = client
	return MongoDB
}

// Get MongoDB instance
func GetDB() *mongo.Client {
	return MongoDB
}
